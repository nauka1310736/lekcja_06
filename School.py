class School:
    def __init__(self, name):
        self.name = name
        
class Class:
    def __init__(self, Class_nr):
        self.Class_nr = Class_nr

        
class Group(Class):
    def __init__(self, Class_nr, Group_name):
        self.Group_name = Group_name


class Person:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name


class Director(Person):
    def __init__(self, first_name, last_name, nr_tel):
        self.nr_tel = nr_tel
        
        
class Teacher(Person):
    def __init__(self, first_name, last_name, subiect):
        self.subiect = subiect


class Student(Person):
    def __init__(self, first_name, last_name, age):
        self.age = age
