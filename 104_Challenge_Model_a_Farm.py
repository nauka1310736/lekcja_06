class Animal:
    def __init__(self, name, age):
        self.name = name
        self.age = age
    def __str__(self):
        return f"{self.name} is {self.age} years old"
    def speak(self, sound):
        return f"{self.name} says {sound}"



class Dog(Animal):
    def speak(self, sound):
        return f"{self.name} says {sound}"

class Cat(Animal):
    def speak(self, sound):
        return f"{self.name} says {sound}"

class Horse(Animal):
    def speak(self, sound):
        return f"{self.name} says {sound}"
